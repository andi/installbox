# Debian Installbox

Ein auf https://salsa.debian.org/installer-team/netboot-assistant basierendes System zur Verteilung von Debian (mit XFCE und noch ein paar anderen Dingen für die tägliche Arbeit) auf "unsere Corona Laptops".

Der Download einer fertigen virtuellen Maschine ist hier möglich: https://cloud.kvfg.de/index.php/s/aXtJQxKccPCm8kX

## Wesentliche Befehlchen

user: installbox 
pasword: muster

sudo su -

Packen des Default Homes für den User kvfg

cd /home ; tar cf /var/lib/tftpboot/d-i/buster/skel.tar kvfg/ ; cd

Nach Änderungen an den Configs:

di-netboot-assistant rebuild-menu


